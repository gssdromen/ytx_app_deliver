package server

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"

	"ytx_app_deliver/api"
)

// NewRouter 路由配置
func NewRouter() *gin.Engine {
	r := gin.Default()
	r.Use(cors.Default())
	// 静态资源文件夹
	r.Static("/static", "./static")
	// 公开资源文件夹，放下载文件
	r.Static("/public", "./public")
	// 网页模板资源
	r.Static("/template", "./templates")

	r.LoadHTMLGlob("templates/*")

	v1 := r.Group("/api/v1")
	{
		v1.POST("app", api.Upload)
		v1.GET("app/:id", api.AppDetail)
	//	v1.GET("app/:id", api.APPList)
	//	v1.GET("app/:id/:folder", api.APPInfo)
	}

	return r
}