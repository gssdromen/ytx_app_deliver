package api

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"ytx_app_deliver/model"
	"ytx_app_deliver/util"
)

// Upload 上传应用IPA或者APK
func AppDetail(c *gin.Context) {
	id := c.Param("id")

	db, _ := util.GetDB()
	defer db.Close()

	var ipa model.IPAModel
	db.Where("unique_id = ?", id).First(&ipa)

	timeStr := ipa.UpdatedAt.Format("2006-01-02 15:04:05")

	var data = util.Struct2Map(ipa)
	path := "api/v1/app/" + id
	data["pageURL"] = util.GetNetworkURLForPath(path)
	data["updateTimeString"] = timeStr

	fmt.Println(timeStr)

	c.HTML(http.StatusOK, "install.html", data)
}