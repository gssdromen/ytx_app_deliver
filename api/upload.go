package api

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/url"
	"os"
	"path"
	"time"
	"ytx_app_deliver/manager"
	"ytx_app_deliver/model"
	"ytx_app_deliver/util"
)

// Upload 上传应用IPA或者APK
func Upload(c *gin.Context) {
	file, err := c.FormFile("file")
	if err != nil {
		fmt.Println(err)
		c.JSON(500, gin.H{
			"status": "error",
			"msg":    "no file",
		})
	}

	randomName := util.RandSeq(5)
	randomFileName := randomName + ".ipa"
	baseTempDir := path.Join("tmp", "files", randomName)
	os.MkdirAll(baseTempDir, 0755)
	ipa := model.IPAModel{}
	ipa.Name = randomFileName
	ipa.UniqueID = randomName

	token := c.PostForm("token")
	if token == "1028ba90-8eef-4ef1-93ff-7c45c0690377" {
		ipa.Type = "Debug"
	} else if token == "36e14df0-8c6f-4619-8f2e-a3fc40cfba1e" {
		ipa.Type = "Release"
	} else {
		ipa.Type = "Unknown"
	}

	c.SaveUploadedFile(file, path.Join(baseTempDir, ipa.Name))

	// 从IPA中取出plist和图标
	plistFilePath, iconFilePath, err := util.UnzipInfoForIPA(path.Join(baseTempDir, ipa.Name), baseTempDir)
	// 读取plist中的信息
	util.GetIPAInfoFromPlist(&ipa, plistFilePath)
	// 把icon放到相关路径
	util.PutIconToRightPath(&ipa, iconFilePath)
	// 上传IPA文件到OSS
	fileURL, _ := util.UploadFileToAliOSS(&ipa, path.Join(baseTempDir, ipa.Name), ipa.Name)
	ipa.NetworkFilePath = fileURL

	timeStr := time.Now().Format("2006-01-02_15-04-05")
	folderPath := path.Join("public", "ipa", ipa.BundleIdentifier, timeStr)

	// 把相关文件放到最终路径
	util.PutIPAToRightPath(&ipa, path.Join(baseTempDir, ipa.Name), plistFilePath, folderPath)

	// 生成安装所需的plist
	installPlistPath, installPlistFileName := util.GenerateInstallPlist(&ipa, folderPath, ipa.Name)
	plistURL, _ := util.UploadFileToAliOSS(&ipa, installPlistPath, installPlistFileName)
	ipa.NetworkInstallPlistPath = plistURL

	// 移动完成后把原来的文件夹删除
	os.RemoveAll(baseTempDir)

	db, err := util.GetDB()

	db.Create(&ipa)

	db.Close()

	// 把网页链接返回过去
	url := url.URL{}
	url.Scheme = manager.GetInstance().Scheme
	url.Host = manager.GetInstance().Host
	url.Path = "api/v1/app/" + ipa.UniqueID

	c.JSON(200, gin.H{
		"status": "ok",
		"result": url.String(),
	})
}