package main

import (
	"fmt"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/spf13/viper"
	"os"
	"ytx_app_deliver/manager"
	"ytx_app_deliver/model"
	"ytx_app_deliver/server"
	"ytx_app_deliver/util"
)

func main() {
	db, err := util.GetDB()

	if err != nil {
		panic(err)
	}

	if !db.HasTable(&model.IPAModel{}) {
		db.CreateTable(&model.IPAModel{})
	}
	db.Close()

	r := server.NewRouter()
	//设置配置文件的名字
	viper.SetConfigName("config")
	//添加配置文件所在的路径
	viper.AddConfigPath(".")
	//设置配置文件类型，可选
	viper.SetConfigType("json")

	err = viper.ReadInConfig()
	if err != nil {
		fmt.Printf("config file error: %s\n", err)
		os.Exit(1)
	}

	port := viper.GetString("network.listenPort")
	manager.GetInstance().Host = viper.GetString("network.outerURL")
	manager.GetInstance().Scheme = viper.GetString("network.scheme")
	manager.GetInstance().OSSEndPoint = viper.GetString("oss.endPoint")
	manager.GetInstance().OSSBucket = viper.GetString("oss.bucket")

	r.Run(port)
}
