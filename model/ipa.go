package model

import "github.com/jinzhu/gorm"

type IPAModel struct {
	gorm.Model
	Name       string
	FolderPath string

	LocalFilePath         string
	LocalPlistPath        string
	LocalIconPath         string
	LocalInstallPlistPath string

	NetworkFilePath  string
	NetworkPlistPath string
	NetworkIconPath  string
	NetworkInstallPlistPath string

	BuildNumber      string
	ShortVersion     string
	BundleIdentifier string
	DisplayName      string

	UniqueID string `gorm:"PRIMARY_KEY"`
	// Debug/Release
	Type string
}
