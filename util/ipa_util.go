package util

import (
	"archive/zip"
	"bytes"
	"errors"
	"fmt"
	"github.com/aliyun/aliyun-oss-go-sdk/oss"
	"io"
	"io/ioutil"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"strings"
	"text/template"
	"ytx_app_deliver/manager"
	"ytx_app_deliver/model"

	plist "howett.net/plist"
)

// UnzipInfoForIPA 从IPA中解压Info.plist, 返回plist和icon路径
func UnzipInfoForIPA(ipaPath string, toPath string) (string, string, error) {
	_, tempName := path.Split(ipaPath)
	reader, err := zip.OpenReader(ipaPath)
	defer reader.Close()
	if err != nil {
		fmt.Println(err)
		return "", "", err
	}

	var plistFile *zip.File
	var iconFile *zip.File
	var size int64

	for _, k := range reader.Reader.File {
		if k.FileInfo().IsDir() == false {
			dir, fileName := filepath.Split(k.Name)
			list := strings.Split(dir, string(filepath.Separator))

			// 上一级目录的文件夹名字最后是.app
			if strings.HasSuffix(list[len(list)-2], ".app") {
				if fileName == "Info.plist" {
					plistFile = k
				}
				if strings.HasPrefix(fileName, "AppIcon") {
					s := k.FileInfo().Size()
					if s > size {
						size = s
						iconFile = k
					}
				}
			}
		}
	}

	if plistFile != nil && iconFile != nil {
		// 先处理plist文件
		reader1, _ := plistFile.Open()
		newPlistFile, err := os.Create(filepath.Join(toPath, tempName+".plist"))
		defer reader1.Close()
		defer newPlistFile.Close()
		if err != nil {
			fmt.Println(err)
			return "", "", err
		}
		io.Copy(newPlistFile, reader1)

		// 再处理icon文件
		reader2, _ := iconFile.Open()
		newIconFile, err := os.Create(filepath.Join(toPath, tempName+".png"))
		defer reader2.Close()
		defer newIconFile.Close()
		if err != nil {
			fmt.Println(err)
			return "", "", err
		}
		io.Copy(newIconFile, reader2)

		return filepath.Join(toPath, tempName+".plist"), filepath.Join(toPath, tempName+".png"), nil
	}
	return "", "", errors.New("Can not find Info.plist in app folder")
}

// GetIPAInfoFromPlist 从plist中获取应用信息
func GetIPAInfoFromPlist(ipa *model.IPAModel, plistPath string) error {
	buf, err := ioutil.ReadFile(plistPath)
	if err != nil {
		return err
	}

	var xval = make(map[string]interface{})
	buf1 := bytes.NewReader(buf)

	decoder := plist.NewDecoder(buf1)
	err = decoder.Decode(&xval)
	if err != nil {
		return err
	}

	identifier := xval["CFBundleIdentifier"]
	if i, ok := identifier.(string); ok {
		ipa.BundleIdentifier = i
		// ipa.AppID = getMD5([]byte(i))[:4]
	}
	displayName := xval["CFBundleDisplayName"]
	if i, ok := displayName.(string); ok {
		ipa.DisplayName = i
	}
	version := xval["CFBundleVersion"]
	if i, ok := version.(string); ok {
		ipa.BuildNumber = i
	}
	shortVersion := xval["CFBundleShortVersionString"]
	if i, ok := shortVersion.(string); ok {
		ipa.ShortVersion = i
	}
	return nil
}

// UploadFileToAliOSS 把文件上传到阿里云
func UploadFileToAliOSS(ipa *model.IPAModel, filePath string, fileName string) (string, error) {
	file, err := os.Open(filePath)
	if err != nil {
		return "", err
	}
	defer file.Close()

	endPoint := manager.GetInstance().OSSEndPoint
	bucketName := manager.GetInstance().OSSBucket

	client, err := oss.New(endPoint, "LTAI4Fp2Wp44X4hZ5vQcy3RC", "gA0K7lKQZQwq4Kfu8CCSarEHLzQxKh")
	if err != nil {
		return "", err
	}

	bucket, err := client.Bucket(bucketName)
	if err != nil {
		return "", err
	}

	err = bucket.PutObject(fileName, file)
	if err != nil {
		return "", err
	}

	url := "https://" + bucketName + "." + endPoint + "/" + fileName

	return url, nil
}

// PutIconToRightPath 把APPICON放到最后的正确位置
func PutIconToRightPath(ipa *model.IPAModel, iconPath string) error {
	// icon
	fileName := ipa.BundleIdentifier + ".png"
	folderPath := path.Join("static", "appicons")

	ipa.LocalIconPath = path.Join(folderPath, fileName)
	ipa.NetworkIconPath = GetNetworkURLForPath(path.Join(folderPath, url.QueryEscape(fileName)))

	os.Rename(iconPath, ipa.LocalIconPath)
	return nil
}

// PutIPAToRightPath 把文件放到最后的正确位置
func PutIPAToRightPath(ipa *model.IPAModel, filePath string, plistPath string, finalFolderPath string) error {
	os.MkdirAll(finalFolderPath, 0755)

	ipa.FolderPath = finalFolderPath
	// 文件
	_, fileName := path.Split(filePath)
	ipa.LocalFilePath = path.Join(finalFolderPath, fileName)
	// plist
	_, fileName = path.Split(plistPath)
	ipa.LocalPlistPath = path.Join(finalFolderPath, fileName)
	ipa.LocalInstallPlistPath = path.Join(finalFolderPath, fileName + "install.plist")
	ipa.NetworkPlistPath = path.Join(finalFolderPath, url.QueryEscape(fileName))

	os.Rename(filePath, ipa.LocalFilePath)
	os.Rename(plistPath, ipa.LocalPlistPath)

	return nil
}

// GenerateInstallPlist 生成安装所需要的plist
func GenerateInstallPlist(ipa *model.IPAModel, folderPath string, name string) (string, string) {
	t, err := template.ParseFiles("static/plist/manifest.plist")
	if err != nil {
		return "", ""
	}
	// 设置模板数据
	data := map[string]interface{}{
		"Name":        ipa.DisplayName,
		"DownloadURL": ipa.NetworkFilePath,
		"IconURL":     ipa.NetworkIconPath,
		"BundleID":    ipa.BundleIdentifier,
		"Version":     ipa.ShortVersion,
	}

	fileName := name + ".install.plist"
	path := path.Join(folderPath, fileName)

	writer, err := os.Create(path)
	defer writer.Close()
	if err != nil {
		return "", ""
	}

	t.Execute(writer, data)

	bytes, err := ioutil.ReadAll(writer)
	if err != nil {
		panic(err)
	}
	fmt.Println("aksddfaskldfjalskdf")
	fmt.Println(string(bytes))

	return path, fileName
}
