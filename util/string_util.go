package util

import (
	"math/rand"
	"time"
	// "net"
	// "fmt"
	// "os"
)

// RandSeq 得到随机字符串
func RandSeq(n int) string {
	letters := []rune("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz")
	b := make([]rune, n)
	r := rand.New(rand.NewSource(time.Now().UnixNano()))

	for i := range b {
		rand := r.Intn(len(letters))
		b[i] = letters[rand]
	}
	return string(b)
}

// GetLocalIP 得到本机的IP
func GetLocalIP() string {
	return "192.168.33.10"
	// addrs, err := net.InterfaceAddrs()
	// if err != nil {
	// 	fmt.Println(err.Error())
	// 	os.Exit(1)
	// }
	// for _, addr := range addrs {
	// 	if ipnet, ok := addr.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
	// 		if ipnet.IP.To4() != nil {
	// 			return ipnet.IP.String()
	// 		}
	// 	}
	// }
	// return ""
}