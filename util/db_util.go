package util

import "github.com/jinzhu/gorm"

func GetDB() (*gorm.DB, error) {
	db, err := gorm.Open("sqlite3", "ytx_app_deliver.db")
	return db, err
}