package util

import (
	"net/url"
	"os"
	"path/filepath"
	"ytx_app_deliver/manager"
)

func GetDirList(path string) ([]string, error) {
	dirList := []string{}

	err := filepath.Walk(path, func(path string, info os.FileInfo, err error) error {
		if info == nil {
			return err
		}
		if info.IsDir() {
			dirList = append(dirList, info.Name())
		}
		return nil
	})

	return dirList, err
}

func GetNetworkURLForPath(path string) string {
	aUrl := url.URL{}
	aUrl.Scheme = manager.GetInstance().Scheme
	aUrl.Host = manager.GetInstance().Host
	aUrl.Path = path

	return aUrl.String()
}