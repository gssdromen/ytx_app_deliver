package manager

import (
	"net/url"
	"sync"
)

type Config struct {
	OSSEndPoint string
	OSSBucket string
	Host string
	Scheme string
}

var instance *Config
var once sync.Once

func GetInstance() *Config {
	once.Do(func() {
		instance = &Config{}
	})
	return instance
}

func (c *Config) GetSelfURL() string {
	aUrl := url.URL{}
	aUrl.Scheme = c.Scheme
	aUrl.Host = c.Host

	return aUrl.String()
}
